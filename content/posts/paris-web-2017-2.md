---
title: "Paris Web 2017 - 2e jour"
date: 2017-10-07T10:32:55+02:00
---

# Jour 2

## L'épopée d'une couleur par Maryla U.

Maryla travaille sur une caméra pour Google à portée culturelle, l'objectif de cet appareil est de numériser des peintures. Et pour nous inviter dans sa réflexion autour de la construction du dispositif, elle a tenté de nous expliquer en 45 minutes comment fonctionne les couleurs et comment celles-ci sont rendus sur nos écrans. Une gageure que Maryla a su relever en décomposant son exposé en plusieurs parties. Moi-même de devoir retranscrire, je sens la plantade.

Une couleur, c'est d'abord une [longueur d'onde](https://fr.wikipedia.org/wiki/Longueur_d%27onde). (BLAM)

Nous pouvons percevoir ces longueurs d'onde grâce à des cônes placées dans le fond de nos yeux. (Je me souviens de mes cours au CNAM où il était question que nous avions plus de facilité à percevoir les bleus car plus de cônes sensibles à cette couleur). Il faut également retenir que nous sommes loin d'appréhender toutes les couleurs possibles. La [crevette-mante](https://fr.wikipedia.org/wiki/Stomatopoda) dispose de 16 cônes ce qui lui permet de distinguer des couleurs dont nous ne pouvons imaginer la représentation.

Nous sommes donc réduits à une certaine gamme de couleurs et à cela il faut ajouter (ou plutôt réduire) la capacité de nos écrans à restituer des couleurs. En effet, les LED disposent de 3 couleurs qui par addition produisent une couleur. Cette restriction des couleurs dûe au matériel est le [gamut](https://fr.wikipedia.org/wiki/Gamut).

À noter qu'on a besoin de calibrer régulièrement son écran, car sa calibration change dans le temps (idem pour les capteurs qui servent à calibrer l'écran).

Le matériel est donc en cause, et les applications me direz-vous ? Eh bien, même si vos images embarquent dans leurs métadonnées des informations quant à l'espace de couleur à utiliser, il n'est pas forcément garanti que le logiciel qui va manipuler les images va réellement le prendre en compte. Le [sRGB](https://fr.wikipedia.org/wiki/SRGB) est un exemple d'espace de couleur.

Dernier prisme (huhu), c'est l'utilisateur. Car oui, nous ne sommes pas égaux face à la restitution des couleurs et donc même si nous arrivons à reproduire fidèlement des couleurs, celles-ci ne seront pas perçus de la même manière en fonction des individus.

Petite remarque perso, la conférencière était vraiment très intéressante, et donne envie de bosser avec elle sur le sujet.

## Why Fast Matters par Harry Roberts

Avec son accent du nord de l'Angleterre, Harry Roberts nous a expliqué la véritable signification du développement d'un site web orienté mobile.
Il ne suffit pas seulement que l'affichage de celui-ci se fasse de manière optimale sur une foultitude de dispositifs, mais il faut également que celui-ci s'affiche le plus rapidement possible. Car nous n'avons pas les mêmes façons d'accéder à internet à travers le monde. 

Une bonne partie de la population mondiale - il était question surtout des pays sud asiatiques - accède à internet seulement via leur téléphone portable. Et non pas en 3G, ni en 4G, mais en EDGE, avec une latence élevée qui force le respect. Un site comme Gmail en version classique met entre 30 secondes et 1 minute à se charger. À comparer avec les performances de nos fibres de pays occidentaux…

Alors comment faire pour vérifier que nos sites se comportent bien dans ces conditions ? D'abord en utilisant un téléphone portable moyennement performant du type Motorola G4 que Harry prenait pour exemple. Ensuite en appliquant une limitation sur la bande passante et la latence. Des outils comme [Charles](https://www.charlesproxy.com/) ou moins complet les outils de développement de Chrome dans la partie réseau.

On doit également vérifier que toutes les ressources chargées par le site web sont effectivement utilisés. L'exemple d'un gestionnaire de tags laisser à l'abandon par l'équipe et qui grevait les performances du site.

Pour monitorer dans le temps et dans des lieux géographiques différents, plusieurs outils : [Dareboost](https://www.dareboost.com/fr/home) ou [Speedcurve](https://speedcurve.com/).

On sent que le conférencier est rodé avec une bonne présence sur scène, et une participation (trop ?) importante du public.

## La sécurité de demain dans le web d'aujourd'hui : mission impossible ? par Aeris

Une conférence d'Aeris (le copain de [PSES](https://passageenseine.fr/)) qui a évoqué les technos à déployer le plus vite possible sur son site web : 

- [HSTS](https://fr.wikipedia.org/wiki/HTTP_Strict_Transport_Security) : force l'utilisation de HTTPS sur un site web.
- [HPKP](https://fr.wikipedia.org/wiki/HTTP_Public_Key_Pinning) : lors de la première connexion à un site web, on présente la liste des certificats autorisés pour le site et leurs durées.
- [DNSSec](https://fr.wikipedia.org/wiki/Domain_Name_System_Security_Extensions) : j'ai bien cru que [Stéphane Bortzmeyer](http://www.bortzmeyer.org/) allait être invoqué à l'évocation de DNSSec. L'idée de ce protocole est de garantir les échanges opérés au niveau du DNS.
- [DANE/TLS](http://www.bortzmeyer.org/6698.html) : DNSSec est un pré-requis à la mise en place de DANE/TLS. Et j'avoue que c'est un protocole que je dois encore creuser. 
- [CSP](https://www.w3.org/TR/CSP/) : des règles qui édictent au site si l'accès à une ressource est autorisé. Ceci à travers des entêtes HTTP.

Beaucoup de ces normes ont été validées jadis, mais encore très peu mises en prod. Il faut faire très attention lorsqu'on déploie HSTS, HPKP, DNSSec ou encore DANE/TLS sous peine d'indisponibilité prolongée (plusieurs jours pour Smashing Mag). L'autre chose à retenir, c'est qu'il faut reprendre le contrôle sur la publication de notre site et éviter de charger des librairies à travers des CDN, par exemple.

## Let's work together par Brad Frost

Comment faire pour mieux travailler ensemble ? Comment éviter que chacun tire la couverture vers soi, car chacun ayant des objectifs différents ? Brad Frost nous explique que la mise en place de guidelines est une réponse à ces questions. Impliquer l'équipe dans sa globalité permet également d'avancer grâce à une concertation des différentes parties impliquées dans un projet.

Les guidelines permettent d'avoir un document de référence que toute l'équipe peut consulter lors de la vie du projet et doit veiller à maintenir.

Moins de choses à dire sur cette conférence, car pris en cours de route, mais on sent que l'orateur est très à l'aise et doit faire énormément ce genre d'exercice.

## CSS, tu peux pas test ! par Thomas Zilliox

Ou la grande difficulté de pouvoir tester les CSS, on peut quand même faciliter avec, pour ce que j'en ai retenu :
- facilité la revue de code avec : [Prettier](https://github.com/prettier/prettier), un linter.
- [BEM](http://getbem.com/introduction/) (ne connait pas, à creuser)
- [Browserstack](https://www.browserstack.com/) pour réaliser des snapshots et vérifier les différences visuelles entre le master et une branche.
- Mettre en place des tests fonctionnels, faciliter avec le découpage du site en composant (philosophie derrière React, Angular, Vue.js).

## Surdité et accessibilité du web par Fanny C.

Sur la question de l'utilisation de malentendant ou sourd. Le mieux reste de voir avec la/les personnes concernées sur le terme à employer.

Je suis désolé pour l'oratrice, mais j'étais en train de gérer quelque chose pour le boulot en même temps et n'ai malheureusement pas retenu plus que ça de la conférence… (pas taper)

## De l'accessibilité vite fait, bien fait par Mylène Chandelier

Plusieurs petites choses à faire lors du développement d'un projet à propos de l'accessibilité :

- alt sur les images (à remplir en fonction du type de l'image).
- sémantique avec aria sur les contenus dynamiques.
- titre sur les onglets.

Je présente également mes plates excuses à l'oratrice, mais dans la continuité de la précédente conférence, je gérais un souci pro.

## De mes 100 échecs, j’ai fait une application de communication universelle par Olivier Jeannel

Olivier Jeannel nous a expliqué à l'oral, et c'est une performance en soi car il est sourd, comment il en est venu à développer son application [RogerVoice](https://rogervoice.com/fr/home) qui permet aux personnes sourdes/malentendantes de pouvoir téléphoner. L'idée est simple, l'application restrancrit à l'écran ce que l'interlocuteur distant dit. Libre à la personne sourde qui appelle soit de répondre à l'oral soit par écrit. L'accent technologique a été porté sur la reconnaissance vocale qui doit retranscrire avec une précision élevée ce qui est dit. En écrivant ces lignes, je me dis qu'on pourrait très bien imaginer une personne entendante utilisée cette application pour conserver une transcription de la discussion. Il faut absolument que je prenne le temps de tester son application.

## Développeur et protection de la vie privée par Erwan Richard

Une prise de conscience de Erwan lors d'une discussion en voiture avec sa moitié, il se rend compte que ce que ces clients lui demandent de mettre en place au quotidien va à l'encontre de la protection des données personnes qui lui est si chère. Une épiphanie. Alors comment fait-on en attendant pour répondre quand même à ce besoin de pouvoir mesure l'audience de nos sites ? C'est surtout en changeant les outils de suivi propriétaires tels [Google Analytics](https://analytics.google.com) pour des solutions open source comme [Piwik](https://piwik.org/). Une autre chose qui va ralentir les ardeurs des clients peu regardants au niveau éthique est la mise en place du RGPD à partir de Mai 2018.

## Le mot de la fin

Un immense #shareTheLove à toute l'équipe de Paris Web pour les conférences de qualité et l'organisation au top. Deux regrets me concernant, ne pas être allé à la rencontre de certaines personnes que je peux suivre sur Twitter, et mon absence aux ateliers du samedi, mais j'essaierai d'y participer l'année prochaine.
