---
title: "Paris Web 2017 - 1er jour"
date: 2017-10-07T10:32:55+02:00
draft: false
---

Pour ma première participation à [Paris Web](https://www.paris-web.fr/), j'ai décidé 2 choses : la première de mettre en place un blog, parce que pourquoi ne pas avoir 15 ans de retard sur tout le monde ? Et ça me permet en même temps d'enfin essayer [Hugo](https://gohugo.io/). La seconde de faire un retour sur cet évènement un peu à part dans le monde des conférences tech.

J'ai tenté quelquechose pour la prise de note, à savoir suivre la conférence sans prendre de notes (?) et résumer cette dernière avec les idées qui m'ont marqué. Il faut savoir que j'avais la crève et que certaines choses ont du me glisser dessus, orateur•rice ne me jetez pas la pierre.

# Jour 1

## le secret de la correspondance numérique par Laurent Chemla et Julien Dudebout.

Démarrage des conférences par l'histoire des correspondances privées de -600 JC à nos jours. Ce que j'en ai retenu, c'est que l'histoire est un éternel recommencement. Depuis l'origine des communications, celles-ci ont toujours été interceptées de manière plus ou moins transparente avec notamment la mise en place du fameux cabinet noir à l'initiative de Richelieu. Les communications sont alors chiffrées pour empêcher la bonne lecture du message, et tombe rapidement l'interdition du chiffrement des messages (Ça me rappelle vaguement quelque chose…). Un éternel recommencement qu'on vous dit. Caliopen, l'arlésienne de Laurent Chemla, et avec l'arrivée récente de Julien Dudebout dans le projet, cherche à démocratiser l'idée de se réaproprier ses données personnelles à travers la création d'un indice de confidentialité. En résumé, l'idée est de faire prendre conscience au grand public de l'enjeu de la vie privée avec de la pédagogie, des outils techniques ergonomiquement plus abordable.

[Caliopen](https://www.caliopen.org/fr/) doit sortir en alpha courant Octobre 2017.

Chose rigolote, c'est que pendant les 2 jours de conférences a circulé sur les réseaux sociaux [l'histoire](https://www.1843magazine.com/technology/rewind/the-crooked-timber-of-humanity) de ces banquiers qui manipulèrent des tours télégraphiques à leur propre compte pour faire circuler les messages qui les intéressaient.

## Alors qu'y a-t-il dans ES2020 ? par Christophe Porteneuve

Avant que les nouvelles fonctionnalités d'ECMA soit intégrées dans la norme, elles passent par 4 stades de validation. Chaque stade de validation prend environ 1 an, ce qui permet à Christophe d'étaler les fonctionnalités jusqu'à 2020 et au delà. Ce que j'ai retenu : 

- Javascript est une marque déposée par Oracle aux US.
- Trailing commas seront permises. Mieux pour les diff !
- Pas mal d'ajout sur les expressions rationnelles : capture groupée, manipulation de chaîne unicode et autres.
- Possibilité de faire des boucles asynchrone. 
- Ajout d'attribut privé, mais vraiment privé.

Pour avoir une vision exhaustive des prochaines fonctionnalités à venir, direction le dépôt [TC39](https://github.com/tc39/proposals).

## La revue de code bienveillante par Gilles Roustan

Un papa qui parle à un papa, ça marche quasi à tous les coups et bien là ça fonctionne. Une de mes conférences coup de cœur à propos des revues de code. Pas mal de choses auxquels j'essaye de penser lors de mes revues de code :
- Se justifier lors d'une demande de modification. "C'est pas une bonne pratique" n'est pas une réponse valable par exemple.
- Être bienveillant, c'est à dire veiller à ne pas blesser le dev.
- Lors d'une demande de mofications du code, toujours mieux de proposer plutôt que de critiquer gratuitement le travail du développeur.
- Toujours plus facile de faire de la revue de code sur une petite partie de code que sur une énorme fonctionnalité.

Je ne connaissais pas le concept de [vitre brisée](https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_de_la_vitre_bris%C3%A9e) qui, si j'ai bien compris, dit qu'on a tendance à laisser quelquechose dans son état. L'exemple est celui de bâtiments non occupés. Un qui est maintenu dans un état correct et l'autre laissé à l'abandon. Celui laissé à l'abandon se dégradera de plus en plus vite, alors que l'autre sera moins dégradé. Remis dans le contexte du développement : si le code est bien structurée, on aura tendance à le laisser dans le même état.

## L'informatique est trop importante pour être laissés aux hommes par Nathalie Pauchet

J'étais très mal en point pendant cette conférence, et je n'ai pas pu tout saisir. Cependant Nathalie nous rappelait que l'informatique a d'abord été un métier où les femmes avaient une grande place et que le marketing l'a orienté petit à petit vers un métier d'hommes. Également qu'on devait se questionner quant à la place de la femme, des minorités au sein de son entreprise. Qu'en France, il y a toujours une grande disparité de salaires entre les hommes et les femmes. Est-ce que la direction est composée de femmes ? Si oui, ça peut donner envie de s'impliquer plus largement dans l'entreprise. 

## Souriez GDPR, PrivacyByDesign, PrivacyByDefault deviennent réalité par Stéphane Lebarque

L'autre sujet tendance en dehors de l'accessibilité cette année à Paris Web, était la GDPR. Stéphane avait la mission (impossible ?) de nous expliquer en 15mins ce qu'apportait le nouveau réglement européen quant à la protection des données personnelles avant Mai 2018.

Après la conférence, j'ai posé la question à Stéphane du recueil du consentement sur les emails avant leur ouverture, car une fois ouvert les données sont trackées. Question que j'avais déjà posée auprès du service juridique du groupe pour lequel je travaille et auquel il n'y a pas forcément de réponse clé en main. D'ailleurs si quelqu'un aurait un début de réponse à ce sujet qu'il n'hésite pas à me contacter.

##UX en agence, 5 ans pour s'y mettre en vrai par Nicolas Le Cam

Retour d'expérience d'une agence sur le changement opéré en interne depuis 5 ans pour proposer des expériences utilisateurs de meilleure qualité auprès de ses clients. Cela passe par :
- une indépendance financière.
- staffer avec des gens expérimentés.
- faire passer le message auprès des clients et …
- … continuer à faire de la pédagogie.

## Changez notre façon de concevoir une API avec GraphQL par Aurélien David

La première conférence technique à laquelle je participe avec les avantages, inconvénients de GraphQL vs Rest API.

Avantages :
- évitez d'avoir plusieurs appels aux endpoints
- pagination, tri et fitre facilités.
- possibilité de faire tourner des applications vieilles de 4 ans.

Inconvénients :
- pas de versionning, on ajoute toujours, compliqué de déprécier certaines parties de l'API.
- cache réseau difficile à mettre en oœuvre.

J'aimerais beaucoup voir la tête que fait la partie GraphQL chez Facebook, j'imagine un énorme plat de spaghetti avec des développeurs qui n'osent plus toucher quoique que ce soit sous peine de tout casser. Autre doute sur l'optimisation des requêtes réseaux en ne demandant qu'à un seul endpoint ce dont on a besoin. En effet, il semble difficile de mettre en cache réseau les requêtes GraphQL, car on peut demander la même chose de plusieurs façons différentes. Alors que sur une API Rest, on peut plus facilement mettre en cache réseau les requêtes. j'imagine qu'il y a un équilibre à trouver entre les deux.

## Les questions oubliées des spécifications : la partie caché de l'iceberg site web par Sylvie Clément

Dans les choses qu'on oublie souvent lors des spécifications d'un projet :
- les notifications par divers canaux de communication (notamment email) lorsqu'on réalise une action particulière sur le site web.
- le community management au sens analyse de l'utilisation faite de la plateforme.

## Informelles sur le traitement des données

Nous n'étions pas nombreux pour participer à cette informelle qui a mis un peu de temps à se mettre en place. C'était sans compter sur un gentil membre du staff de Paris Web (Julien Wajsberg, pour ne pas le nommer) qui est venu engager la conversation avec chacun. La personne à l'initiative de l'informelle cherchait à résoudre une problématique d'Ajax et PHP. En effet, le script PHP interrogé en Ajax ne retournait la sortie que par paquet. Après avoir sorti la grosse bertha en parlant des Server Sent Events (SSE), il devait très certainement s'agir soit de la bufferisation d'Apache, soit de PHP (voire les deux) et que cela pouvait être résolu à coup de fonction ob_* (ob_end_flush, ob_start, etc…).

## "C'est la faute de l'utilisateur" - où est passée l'empathie de mon équipe ? par My Lê

En plus d'avoir pris en cours cette conférence et d'être au quatrième sous-sol pendant cette conférence, je n'ai malheureusemet pas retenu grand chose si ce n'est qu'on devrait plus souvent se remettre en question lorsqu'un utilisateur rencontre des difficultés face à une application. Ce n'est pas systématiquement la faute de l'utilisateur.

## Design BBC Account : with Big Data comes big responsibility ? par Cyrièlle Piancastelli

Parcours pour mettre en oœuvre un projet d'IAM à travers tous les services de la BBC avec :
- récolte des data nécessaires pour que les services fonctionnent comme auparavant
- développement du projet en tant que tel.
- mise en place d'un guideline pour que les services de la BBC puisse intégrer le nouveau projet chez eux.

Beaucoup de mal à suivre cette conférence, non pas à cause de la crève cette fois-ci, mais pour plusieurs petites choses. Le franglais qui devient vite difficile à suivre et la mise en avant du parcours de l'oratrice pendant toute la première partie de la conf sans pour autant que je vois où ça pouvait servir le propos de la conf.

## Projet perso n°42 : une aventure ultrasonique par Hubert Sablonnière

Une autre conférence coup de coœur, avec des messages qui devraient passer auprès des recruteurs. Arrêtons de faire une course à l'échalotte sur les projects persos : "Quoi tu n'en fais pas ? Tu as une autre vie à côté du dév ? Tu n'as pas 10000 commits sur github ?".
Et en même temps, l'orateur montre un project perso qui tue. Le projet est une visionneuse de transparents type Powerpoint. Pas folichon dit comme ça, mais l'idée est d'avoir les notes de la présentation sur un portable, et la présentation sur un autre portable. Les deux PC doivent rester en synchronisation lorsqu'on avance dans la présentation ce qui lui a permis de mettre en œuvre WebRTC avec comme contrainte de ne pas mettre en place de serveur intermédiaire pour faire la transmission des messages entre les 2 machines. Ce qui, si j'ai bien compris, n'a  pas été possible au final.
Le projet n'est pas fini, mais comme le disait Hubert : "ce n'est pas grave, c'est un projet perso".

Une autre chose que j'ai retenu, c'est le concept d'Ikigai et parce qu'une image vaut souvent mieux qu'un long discours :

![alt text](http://laetthesunshine.com/wp-content/uploads/2016/11/IKIGAI-Blog.jpg "Ikigai")

L'idée est de tendre vers l'Ikigai, c'est un concept que je me note à creuser.