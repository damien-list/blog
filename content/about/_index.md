---
title: "À propos"
featured_image: ''
---
Je m'appelle Damien List, passionné d'informatiques : développement, administration système, sécurité, etc… En dehors de ma passion pour l'informatique, je suis un grand lecteur de BD/comics/roman graphique. Amateur également de jeux vidéo avec une prédilection pour les jeux de rôle et d'aventure (entre autres), de jeux de plateau, de musique et de cinéma.

Côté professionnel, je travaille chez [Vertical-Mail](https://www.vertical-mail.com) en tant que Directeur technique depuis maintenant 10 ans.